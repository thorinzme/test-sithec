using Microsoft.EntityFrameworkCore;
using System.Data;
namespace Models
{
    
    public class HumanoContext : DbContext
    {
        public HumanoContext(DbContextOptions<HumanoContext> options) : base(options) {}
        public DbSet<Humano> humanos {get; set;}
        public DbSet<Sexo> sexo {get; set;}


        
    }
}