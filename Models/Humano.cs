using System;
using System.Collections.Generic;
namespace Models
{
    public class Humano
    {
        public int? Id {get;set;}
        public string Nombre { get; set; }
        public int SexoId { get; set; }
        public Sexo Sexo { get; set; }
        public int Edad {get;set;}
        public double Altura {get;set;}
        public double Peso {get;set;}
    }
    public class Sexo
    {
        public int? Id {get;set;}
        public string Nombre { get; set; }
        public ICollection<Humano> Humano { get; set; }

    }
}
