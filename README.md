El proyecto se desarrolló con dotnet 3.1.422
-Antes de iniciar el proyecto se deberá tener una base de datos postgres, 
 se recomienda usar docker sudo docker run--namepostgresql-p5432:5432 -e POSTGRES_PASSWOR=yourpass-d postgres

-Se debe capturar la información de la BD
 creada en la cadena de conexión del archivo appsettings.json

-Se debe agregar la migración de las tablas con el siguiente comando    
dotnet ef database update

-Ahora se puede correr el proyecto con el siguiente commando  
dotnet run 

Se puede probar la apitomando la colección de postman de la siguiente ruta
https://www.getpostman.com/collections/1e5c40b5898c17092599
