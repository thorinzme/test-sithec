using System;

namespace Controllers.ViewModels
{
    public class ViewHumano
    {
        public int? Id {get;set;}
        public string Nombre { get; set; }
        public String Sexo { get; set; }
        public int Edad {get;set;}
        public double Altura {get;set;}
        public double Peso {get;set;}
    }
}
