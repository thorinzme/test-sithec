using System;

namespace Controllers.ViewModels
{
    public class Datos
    {
        public double Valor1 {get;set;}
        public double Valor2 {get;set;}
        public double Valor3 {get;set;}

        public string Suma()
        {
            return string.Format("La suma de los elementos es: {0}", Valor1 + Valor2 + Valor3);
        }

        public string Resta()
        {
            return string.Format("La resta de los elementos es: {0}", Valor1 - Valor2 - Valor3);
        }

        public string Multiplicacion()
        {
            return string.Format("La multiplicacion de los elementos es: {0}", Valor1 * Valor2 * Valor3);
        }

        public string Division()
        {
            return string.Format("La division de los elementos es: {}", Valor1 / Valor2 / Valor3);
        }
    }

}
