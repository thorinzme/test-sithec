using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Models;
using Microsoft.Extensions.Configuration;
using System.Data;
using Npgsql;
using Controllers.ViewModels;

namespace Test.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class HumanoController : ControllerBase
    {
        private readonly IConfiguration _configuration;
        private readonly ILogger<HumanoController> _logger;

        private readonly HumanoContext _context;


        public HumanoController( IConfiguration configuration,  ILogger<HumanoController> logger, HumanoContext context)
        {
            _logger = logger;
            _configuration = configuration;
            _context = context;
        }

        [HttpGet("mock")]
        public JsonResult Get()
        {   

            
            var arrayHumanos =  new ViewHumano[2];
            arrayHumanos[0] = new ViewHumano(){
                                Id = 1,
                                Nombre = "Nombre1",
                                Sexo = "Alguno",
                                Edad = 18,
                                Peso = 80.0,
                                Altura = 180
                            };

            arrayHumanos[1] = new ViewHumano(){
                                Id = 2,
                                Nombre = "Nombre2",
                                Sexo = "Alguno",
                                Edad = 18,
                                Peso = 80.0,
                                Altura = 180
                            };  
            
            return new JsonResult(arrayHumanos);
        }

        [HttpGet()]
        public JsonResult GetAll()
        {   

            List<ViewHumano> listHumano = new List<ViewHumano> ();
            var listSexo = _context.sexo.ToList();
            foreach(var row in _context.humanos.ToList()){
                listHumano.Add( new ViewHumano(){
                            Id = row.Id,
                            Nombre = row.Nombre,
                            Sexo = listSexo.Where(sex => sex.Id == row.SexoId).FirstOrDefault().Nombre,
                            Edad = row.Edad,
                            Peso = row.Peso,
                            Altura = row.Altura
                        });
            }
            return new JsonResult(listHumano);

            /*string query = @"
                select H.id_humano as ""Id"",
                        H.nombre as ""Nombre"",
                        S.nombre as ""Sexo"",
                        H.edad as ""Edad"",
                        H.altura as ""Altura"",
                        H.peso as ""Peso""

                from test.humano as H
                left join test.sexo as S on H.id_sexo = S.id_sexo
            ";

            DataTable table = new DataTable();

            string sqlDataSource = _configuration.GetConnectionString("HumanoContext");

            NpgsqlDataReader myReader;
            using (NpgsqlConnection myCon = new NpgsqlConnection(sqlDataSource))
            {
                myCon.Open();
                using (NpgsqlCommand myCommand = new NpgsqlCommand(query, myCon))
                {
                    myReader = myCommand.ExecuteReader();
                    table.Load(myReader);
                    myReader.Close();
                    myCon.Close();
                }
            }

            return new JsonResult(table);*/

        }

        [HttpPost]
        public JsonResult Post(Humano humano)
        {
            _context.humanos.Add(humano);

            try
            {
                _context.SaveChanges();
                return new JsonResult(string.Format("Se creo el registro con id {0}", humano.Id)) ;
            }
            catch (Exception ex)
            {
                return new JsonResult(ex);
            }

            

            /*string query = @"
                insert into humano(nombre, sexo_id, edad, altura, peso)
                values (@nombre, @sexo_id, @edad, @altura, @peso)
            ";

            DataTable table = new DataTable();
            string sqlDataSource = _configuration.GetConnectionString("HumanoContext");
            NpgsqlDataReader myReader;
            using (NpgsqlConnection myCon = new NpgsqlConnection(sqlDataSource))
            {
                myCon.Open();
                using (NpgsqlCommand myCommand = new NpgsqlCommand(query, myCon))
                {
                    myCommand.Parameters.AddWithValue("@nombre", humano.Nombre);
                    myCommand.Parameters.AddWithValue("@id_sexo", humano.Sexo.Id);
                    myCommand.Parameters.AddWithValue("@edad", humano.Edad);
                    myCommand.Parameters.AddWithValue("@altura", humano.Altura);
                    myCommand.Parameters.AddWithValue("@peso", humano.Peso);

                    myReader = myCommand.ExecuteReader();

                    table.Load(myReader);

                    myReader.Close();
                    myCon.Close();

                }
            }

            return new JsonResult("Agregado correctamente");*/
        }

        [HttpGet("{idHumano}")]
        public JsonResult Get(int idHumano)
        {

            var humano = _context.humanos.Find(idHumano);
            if( humano == null )
                return new JsonResult("No se pudo obtener el registro");

            var sexo =  _context.sexo.Find(humano.SexoId);
            if( sexo == null )
                return new JsonResult("No se pudo obtener el registro");
            
            var view = new ViewHumano(){
                            Id = humano.Id,
                            Nombre = humano.Nombre,
                            Sexo = sexo.Nombre,
                            Edad = humano.Edad,
                            Peso = humano.Peso,
                            Altura = humano.Altura
            };

            return new JsonResult(view);
            
            /* string query = @"
                select H.id as ""Id"",
                        H.nombre as ""Nombre"",
                        S.nombre as ""Sexo"",
                        H.edad as ""Edad"",
                        H.altura as ""Altura"",
                        H.peso as ""Peso""

                from humanos as H
                left join sexo as S on H.sexo_id = S.id
                where H.id = @id_humano
            ";

            DataTable table = new DataTable();
            string sqlDataSource = _configuration.GetConnectionString("HumanoContext");
            NpgsqlDataReader myReader;
            using (NpgsqlConnection myCon = new NpgsqlConnection(sqlDataSource))
            {
                myCon.Open();
                using (NpgsqlCommand myCommand = new NpgsqlCommand(query, myCon))
                {
                    myCommand.Parameters.AddWithValue("@id_humano", idHumano);
                    myReader = myCommand.ExecuteReader();

                    table.Load(myReader);

                    myReader.Close();
                    myCon.Close();

                }
            }

            return new JsonResult(table);*/
        }

        [HttpPut]
        public JsonResult Put(Humano humano)
        {
            _context.humanos.Update(humano);
            
            try
            {
                 _context.SaveChanges();
                 return new JsonResult(string.Format("Se actualizo el registro con id {0}", humano.Id)) ;
            }
            catch (Exception ex)
            {
                if (  _context.humanos.Find(humano.Id) == null)
                {
                    return new JsonResult(string.Format("El registro con id {0} no existe", humano.Id));
                }
                else
                {
                    return new JsonResult(string.Format("{0}", ex));
                }
            }
           

          
            /*string query = @"
                update test.humano
                set nombre = @nombre,
                    id_sexo = @id_sexo,
                    edad = @edad,
                    altura = @altura,
                    peso = @peso
                    where id_humano = @id_humano
            ";

            DataTable table = new DataTable();
            string sqlDataSource = _configuration.GetConnectionString("HumanoContext");
            NpgsqlDataReader myReader;
            using (NpgsqlConnection myCon = new NpgsqlConnection(sqlDataSource))
            {
                myCon.Open();
                using (NpgsqlCommand myCommand = new NpgsqlCommand(query, myCon))
                {
                    myCommand.Parameters.AddWithValue("@id_humano", humano.Id);
                    myCommand.Parameters.AddWithValue("@nombre", humano.Nombre);
                    //myCommand.Parameters.AddWithValue("@id_sexo", humano.IdSexo);
                    myCommand.Parameters.AddWithValue("@edad", humano.Edad);
                    myCommand.Parameters.AddWithValue("@altura", humano.Altura);
                    myCommand.Parameters.AddWithValue("@peso", humano.Peso);

                    myReader = myCommand.ExecuteReader();

                    table.Load(myReader);

                    myReader.Close();
                    myCon.Close();

                }
            }
            return new JsonResult("Actualizado correctamente");*/
        }
    }
}
