using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Models;
using Controllers.ViewModels;
namespace Test.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class OperacionController : ControllerBase
    {

        private readonly ILogger<OperacionController> _logger;

        public OperacionController(ILogger<OperacionController> logger)
        {
            _logger = logger;
        }

        [HttpPost("suma")]
        public string Suma(Datos datos)
        {   
            return datos.Suma();
        }

        [HttpPost("multiplicacion")]
        public string Multiplicacion(Datos datos)
        {   
            return datos.Multiplicacion();
        }

        [HttpGet("suma/{valor1}/{valor2}/{valor3}")]
        public string Suma(double valor1, double valor2, double valor3)
        {   
            var res = new Datos(){
                Valor1 = valor1,
                Valor2 = valor2,
                Valor3 = valor3
            };
            return res.Suma();
        }

        [HttpGet("multiplicacion/{valor1}/{valor2}/{valor3}")]
        public string Multiplicacion(double valor1, double valor2, double valor3)
        {   
            var res = new Datos(){
                Valor1 = valor1,
                Valor2 = valor2,
                Valor3 = valor3
            };
            return res.Multiplicacion();
        }
        
    }
}
